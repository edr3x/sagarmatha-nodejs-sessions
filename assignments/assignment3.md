## Assignment 3

1. Different examples of desctructuring (3)
2. Spread and rest operator examples (3)
  a. Pass infinite no of arguments to a function
  b. Copy properties of an object
  c. Copy values of an array
3. Convert previous work to arrow function
4. Create a file called main.js and files for above task. Only create data and functions on the previous 3 files and print the values from main.js only.
