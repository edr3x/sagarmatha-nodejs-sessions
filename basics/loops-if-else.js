const num = 0;

if (num > 0) {
  console.log('positive');
} else if (num < 0) {
  console.log('negative');
} else {
  var val = 123;
  console.log('zero');
}

console.log(val);

for (var i = 0; i < 10; i++) {
  if (i % 2 === 0) console.log('EVEN', i, 'hello');
  else console.log('ODD', i);
}

console.log(i);
